﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.Extensions.Logging;
using System;
using Umbraco.Cms.Core.Models;
using Umbraco.Cms.Core.Web;
using Umbraco.Cms.Web.Common.Controllers;

namespace Project.core
{
    public class abctestController : RenderController
    {
        private readonly ILogger<RenderController> _logger;
        private static int _counter = 0;
        public abctestController(ILogger<RenderController> logger, ICompositeViewEngine compositeViewEngine, IUmbracoContextAccessor umbracoContextAccessor) : base(logger, compositeViewEngine, umbracoContextAccessor)
        {
            _logger = logger;
        }
        public override IActionResult Index()
        {
            _logger.Log(LogLevel.Information, $"Visit count: {++_counter} & value for hello: {HttpContext.Request.Query["hello"]}");
            return CurrentTemplate(new ContentModel(CurrentPage)); ;
        }

    }

}
